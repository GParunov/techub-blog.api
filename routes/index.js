const routes = require("express").Router();
const lists = require("./lists");
const user = require("./user")

routes.use("/lists", lists);
routes.use("/user", user)

routes.get("/", (req, res) => {
  res.status(200).json({ message: "Connected!" });
});

module.exports = routes;

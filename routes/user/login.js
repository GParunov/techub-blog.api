const fs = require('fs');

module.exports = (req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    fs.readFile('database.json', (err, data) => {
        if(err) throw err;

        let database = JSON.parse(data);
        let registereduser = database.filter( (item) => {
            return item.username == username && item.password == password;
        }) [0]

        if(!registereduser) {
            res.json({
                status : 404,
                message : 'Student not found'
            })
        } else {
            res.json({
                status : 200,
                message : 'Student found'
            })
        }
    })
  };

const user = require("express").Router();
const login = require("./login");
// const logout = require("./logout");
// const registration = require("./registration");
// const forget = require("./forget");
// const change = require("./change");
// const deleted = require("./deleted");

const bodyParser = require('body-parser');
user.use(bodyParser.json());



user.post("/login", login);
// user.get("/logout", logout);
// user.get("/registration", registration);
// user.get("/forget", forget);
// user.get("/change", change);
// user.get("/deleted", deleted);


user.get("/", (req, res) => {
  res.json({
    message: "User Routers"
  });
});

module.exports = user;
const lists = require("express").Router();
const popular = require("./popular");
const featured = require("./featured");

lists.get("/popular", popular);
lists.get("/featured", featured);

lists.get("/", (req, res) => {
  res.json({
    message: "Lists Routers"
  });
});

module.exports = lists;

const express = require("express");
const PORT = process.env.PORT || 4200;
const app = express();
const routes = require("./routes");

app.use("/", routes);

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
